# parlA2 - bootstrapping parallel language
#
# Dedicated to the Life.
#
# The initial aim is to be a practical combination of features from previous
# prototypes:
#
# parl  - combined iterators with |, also contained some parsing stuff
# parle - combined functions with *, rows and tables, function rows
# parlA - ⊥ & and |, interpreted λ
#
# plus the addition of interprocess communication and interoperability,
#
# with the view to using it for bootstrapping
#
# Motivating use cases:
#   - parlAC - like parlA2 but entirely written in parlAC
#
#   - scriptural analysis software
#
# Except for the use of SIDE <https://www.gitlab.com/dlovemore/side> I'm doing
# this mostly from scratch.
#
# We're not stopping at "working code" but continuing to work the code until
# some level of competence is attained. And we're going round this cycle of
# more or less starting from scratch until what we have seems somehow good.
#
# And to begin I'm going to try and rewrite SIDE within parlA2.
#
# So we're going to need piping and splitting and combining streams of data.
#
# Also, I'm investigating the idea of using ⊥ to encapsulate continuations.
#
# ⊥ I have been experimenting with in parlA as a form of exceptions which
# bridge the gap between data and control flow.
#
# Now it's tempting at this point to use clever metaclass stuff to generate
# classes.  Perhaps making an extension library to python3 that works with
# python3 well is a good thing to do. But here my motivation is toward
# portability, bootstrapping, and independence of target language.
#
# So I'm going to move away from highly specific language features, which I
# would tend to anyway, and try and do everything through a single class.
#
# So why a single class?
#
# We're initially leaning on language features for overloading so we're going
# to need at least one class.
#
# And we're implementing the class logic ourselves because that's what we're
# going to need to do ultimately
#
# So we are also going to end up expressing all the type and dispatch logic
# through this class as well.
#
# This makes me ask, why use Python at all? Well, it's giving us basic data
# types reading and parsing as well as access to system programming and other
# useful libraries for shims.
#
# But let's back up and review the use of python for prototyping a simple
# language, which we will probably later break out into a separate document.
#
#
# PROGRAMMING LANGUAGE PROTOTYPING IN PYTHON
#
# Python 3 allows defining that allows redefing operators such as
#
#     >>> import operator
#     >>> ' '.join(dir(operator))
#     '__abs__ __add__ __all__ __and__ __builtins__ __cached__ __concat__ __contains__ __delitem__ __doc__ __eq__ __file__ __floordiv__ __ge__ __getitem__ __gt__ __iadd__ __iand__ __iconcat__ __ifloordiv__ __ilshift__ __imatmul__ __imod__ __imul__ __index__ __inv__ __invert__ __ior__ __ipow__ __irshift__ __isub__ __itruediv__ __ixor__ __le__ __loader__ __lshift__ __lt__ __matmul__ __mod__ __mul__ __name__ __ne__ __neg__ __not__ __or__ __package__ __pos__ __pow__ __rshift__ __setitem__ __spec__ __sub__ __truediv__ __xor__ _abs abs add and_ attrgetter concat contains countOf delitem eq floordiv ge getitem gt iadd iand iconcat ifloordiv ilshift imatmul imod imul index indexOf inv invert ior ipow irshift is_ is_not isub itemgetter itruediv ixor le length_hint lshift lt matmul methodcaller mod mul ne neg not_ or_ pos pow rshift setitem sub truediv truth xor'
#     >>> 
#
# __add__ for example defines +
#
# For example, tuples add with +, but we cannot add elements with +
#
#     >>> (1,2,3)+(7,8,9)
#     (1, 2, 3, 7, 8, 9)
#     >>> (1,2,3)+4
#     <5>:1: TypeError: can only concatenate tuple (not "int") to tuple
#     >>> 
#
# But I can make my own type Row that does so.
#
#     >>> class Row(tuple):
#     ...     def __new__(cls, *values):
#     ...         return super().__new__(cls, values)
#     ...     def __add__(S, rhs):
#     ...         if isinstance(rhs,Row): return Row(*(tuple(S)+rhs))
#     ...         return Row(*(tuple(S)+(rhs,)))
#     ... 
#
# I'm using S instead of self, because I think it's easier to read.
# The tuple(S) is for adding in the tuple base type to avoid recursion.
# 
#     >>> Row(1,2,3)
#     (1, 2, 3)
#     >>> Row(1,2,3)+Row(7,8,9)
#     (1, 2, 3, 7, 8, 9)
#     >>> Row(1,2,3)+4
#     (1, 2, 3, 4)
#     >>> 
#
# Python also allows us to change the ways values are displayed.
#
#     >>> Row1=Row
#     >>> class Row(Row1):
#     ...     def __repr__(S): return type(S).__name__+super().__repr__()
#     ... 
#     >>> 
#     >>> Row(1,2,3)
#     Row(1, 2, 3)
#     >>> repr(_)
#     'Row(1, 2, 3)'
#     >>> eval(_)
#     Row(1, 2, 3)
#     >>> 
#
# repr() attempts to turn the object into a printable string that can be
# reparsed to make the same value. This is not enforced in Python.
#
# You can read about str() and repr() in the python docs.
#
# Concatenating the tuple repr with the type name is a bit of a kluge. This can
# be also done with something like:
#
#     >>> s=', '.join((str(x) for x in (1,2,3)))
#     >>> '('+s+')'
#     '(1, 2, 3)'
#     >>> '(%s)' % s  # using % operator on strings
#     '(1, 2, 3)'
#     >>> f'({s})' # f strings are from about Python 3.7. ∃ pros and cons.
#     '(1, 2, 3)'
#     >>> 
#
# Code Reuse
#
# I'm redefining a new class instead of modifying the code for the previous
# class Row as part of a reuse discipline.
#
# To properly reuse code, we need to be able to reuse it as a component. The
# discipline has helped me to understand what makes reuse properly work.
#
# It's an informative thing to try, but I'm not being rigid about it here.
#
# Anyway, an important innovation is constructing functions by composition. For
# better or worse, I'm overloading the use of * (by providing __mul__) for
# function application.
#
# If we have data x, y and z and functions f,g,h then:
#
# x*f means apply f to x i.e. f(x)
# f*g means apply f then g i.e. (f*g)(x) = g(f(x))
#
# f*x is yet to be deliberately designed. We'll see if we can make one later.
# But previously I've used this for a right hand argument of a binary operator.
#
#     >>> import functools
#     >>> class Action:
#     ...     def __init__(S, fn, name):
#     ...         S.f=fn
#     ...         S.name=name
#     ...     def __rmul__(S, lhs):
#     ...         return S.f(lhs)
#     ...     def __mul__(S, rhs):
#     ...         return Action(compose(S.f,rhs.f), f'({S}*{rhs})')
#     ...     def __repr__(S): return S.name
#     ... 
#     >>> def compose(f,g): # f then g
#     ...     def gf(*args,**kwargs):
#     ...         return g(f(*args,**kwargs))
#     ...     return gf
#     ... 
#     >>> def F(fn): return Action(fn,fn.__name__)
#     ... 
#     >>> words=Action(lambda x:x.split(' '), 'words')
#     >>> "In the beginning"*words
#     ['In', 'the', 'beginning']
#     >>> Len=F(len)
#     >>> "In the beginning"*words*Len
#     3
#     >>> wordcount=words*Len
#     >>> "In the beginning"*wordcount
#     3
#     >>> 
#
# __rmul__ gets called when __mul__ is not implemented. You can read about the
# details in the python docs on the data model.
# 
# Approximately, though:
#
# data*f becomes operator.mul(data,f)
#
# which first tries data.__mul__(f) then f.__rmul__(data)
#
# It's a kind of weirdness that we can't even write compose in a natural
# way in python. What is the type of an argument list? It's a bit like a tuple
# but then with keyword args. But it's more complicated than that and the
# definition varies according to the version of python. And why is it not
# straightforwardly represented as a value in the language?
#
# Importantly though, how are we defining an argument type?
#
# I need to be clear when I write 1*f whether I am doing f(1) or f((1,))
#
# Similarly (1,2)*f is that f(1,2) or f((1,2))?
#
# It looks like we're basically doing f(1) in the first case and f((1,2)) in
# the second. By accident though rather than design.
#
# Oh, and perhaps the most useful thing to do is override __call__ that makes
# an object behave like a function. That is an exercise for the reader.
# 
# So this is the sort of thing that can be done by defining classes.
#
# But we are going to try and do everything through a single class allowing us
# to make new classes programmatically with using exec and being pulled into
# generating strings that look like python code. It could be done that way,
# though.
#
# So we can make a method for __getattr__ that allows us to supply missing
# attributes. But this doesn't work for special methods, partly for
# optimisation reasons.
#
# So we're undoing all that work and doing all dispatch through a single
# function to regain flexibilty and control.
#
#     >>> class O:
#     ...     def __getattr__(S,k):
#     ...         return dot(S,k)
#     ...     def __mul__(S,rhs):
#     ...         return S.__getattr__('__mul__')(rhs)
#     ... 
#     >>> def dot(o,k): return (id(o),k)
#     ... 
#     >>> x=O()
#     >>> x.a
#     (4145208240, 'a')
#     >>> 
#     >>> def dot(o,k): return (o,k)
#     ... 
#     >>> y=O()
#     >>> y.hi
#     (<__console__.O object at 0xf712da90>, 'hi')
#     >>> attrs={}
#     >>> def dot(o,k): return attrs[k] if k in attrs else (o,k)
#     ... 
#     >>> attrs['x']=1
#     >>> y.x
#     1
#     >>> 
#
# There's some decisions that may need revising about handling special
# methods.  We need to make some attempt to interoperate with python
# properly but ultimately we are trying to bootstrap so
# interoperability at this layer isn't currently our main objective.
#
# However there are subtleties that can bite us if we don't get them
# right.
#
# One such subtlety is cleaning up dead objects. Normally object
# values die with the object, but if we put them somewhere else, then
# we need to tidy them up when the object dies.
#
# WeakKeyDictionary from weakref library may come in useful.
#

# The order of methods in O2 below attempts to follow:
# <https://docs.python.org/3/reference/datamodel.html>
class O2:
    def __getattr__(S,key): return O2sm(S,'__getattr__',key)
    # __getattribute__(self,name)
    # __slots__=()
    # With no slots and no __getattribute__ all getattr()s go through
    # the __getattr__ method.
    # Other special methods all go through O2sm()
    ##
    def __setattr__(S,key,val): return O2sm(S,'__setattr__',key,val)
    def __delattr__(S,key):     return O2sm(S,'__delattr__',key)
    ##
    # def __new__(cls,*args,**kwargs): return O2new(*args,*kwargs)
    # def __init__ (S, *args): O2sm(S,'__init__',*args)
    # __del__
    def __repr__  (S):   return O2sm(S,'__repr__')
    def __str__   (S):   return O2sm(S,'__str__')
    def __bytes__ (S):   return O2sm(S,'__bytes__')
    def __format__(S,spec): return O2sm(S,'__format__',spec)
    def __lt__(S,other): return O2sm(S,'__lt__',other)
    def __le__(S,other): return O2sm(S,'__le__',other)
    # def __eq__(S,other): return O2sm(S,'__eq__',other)
    def __ne__(S,other): return O2sm(S,'__ne__',other)
    def __gt__(S,other): return O2sm(S,'__gt__',other)
    def __ge__(S,other): return O2sm(S,'__ge__',other)
    ##
    # def __hash__(S):     return O2sm(S,'__hash__')
    def __bool__(S):     return O2sm(S,'__bool__')
    def __dir__ (S):     return O2sm(S,'__dir__')
    # __getattr__, __getattribute__, __setattr__, __delattr__
    # __get__(self, instance, owner=None)
    # __set__(self, instance, value)
    # def __delete__(S,instance): return O2sm(S,'__delete__',instance)
    ##
    def __call__(S,*args,**kwargs): return O2sm(S,*args,**kwargs)
    def __len__(S): return O2sm(S,'__len__')
    def __length_hint__(S): return O2sm(S,'__length_hint__')
    def __getitem__(S,key): return O2sm(S,'__getitem__',key)
    def __setitem__(S,key,val): return O2sm(S,'__setitem__',key,val)
    def __delitem__(S,key): return O2sm(S,'__delitem__',key)
    # def __missing__(S,key): return O2sm(S,'__missing__',key)
    # def __iter__(S): return O2sm(S,'__iter__')
    # def __reversed__(S): return O2sm(S,'__reversed__')
    ##
    def __contains__(S,item):  return O2sm(S,'__contains__',item)
    def __add__      (S,other): return O2sm(S, '__add__',     other)
    def __sub__      (S,other): return O2sm(S, '__sub__',     other)
    def __mul__      (S,other): return O2sm(S, '__mul__',     other)
    def __matmul__   (S,other): return O2sm(S, '__matmul__',  other)
    def __truediv__  (S,other): return O2sm(S, '__truediv__', other)
    def __floordiv__ (S,other): return O2sm(S, '__floordiv__',other)
    def __mod__      (S,other): return O2sm(S, '__mod__',     other)
    def __divmod__   (S,other): return O2sm(S, '__divmod__',  other)
    def __pow__      (S,*args): return O2sm(S, '__pow__',     *args)
    def __lshift__   (S,other): return O2sm(S, '__lshift__',  other)
    def __rshift__   (S,other): return O2sm(S, '__rshift__',  other)
    def __and__      (S,other): return O2sm(S, '__and__',     other)
    def __xor__      (S,other): return O2sm(S, '__xor__',     other)
    def __or__       (S,other): return O2sm(S, '__or__',      other)
    ##
    def __radd__     (S,other): return O2sm(S,'__radd__',     other)
    def __rsub__     (S,other): return O2sm(S,'__rsub__',     other)
    def __rmul__     (S,other): return O2sm(S,'__rmul__',     other)
    def __rmatmul__  (S,other): return O2sm(S,'__rmatmul__',  other)
    def __rtruediv__ (S,other): return O2sm(S,'__rtruediv__', other)
    def __rfloordiv__(S,other): return O2sm(S,'__rfloordiv__',other)
    def __rmod__     (S,other): return O2sm(S,'__rmod__',     other)
    def __rdivmod__  (S,other): return O2sm(S,'__rdivmod__',  other)
    def __rpow__     (S,*args): return O2sm(S,'__rpow__',     *args)
    def __rlshift__  (S,other): return O2sm(S,'__rlshift__',  other)
    def __rrshift__  (S,other): return O2sm(S,'__rrshift__',  other)
    def __rand__     (S,other): return O2sm(S,'__rand__',     other)
    def __rxor__     (S,other): return O2sm(S,'__rxor__',     other)
    def __ror__      (S,other): return O2sm(S,'__ror__',      other)
    ##
    # def __iadd__     (S,other): return O2sm(S,'__iadd__',other)
    # def __isub__     (S,other): return O2sm(S,'__isub__',other)
    # def __imul__     (S,other): return O2sm(S,'__imul__',other)
    # def __imatmul__  (S,other): return O2sm(S,'__imatmul__',other)
    # def __itruediv__ (S,other): return O2sm(S,'__itruediv__', other)
    # def __ifloordiv__(S,other): return O2sm(S,'__ifloordiv__',other)
    # def __imod__     (S,other): return O2sm(S,'__imod__',other)
    # def __ipow__     (S,*args): return O2sm(S,'__ipow__',*args)
    # def __ilshift__  (S,other): return O2sm(S,'__ilshift__',other)
    # def __irshift__  (S,other): return O2sm(S,'__irshift__',other)
    # def __iand__     (S,other): return O2sm(S,'__iand__',other)
    # def __ixor__     (S,other): return O2sm(S,'__ixor__',other)
    # def __ior__      (S,other): return O2sm(S,'__ior__',other)
    ##
    def __neg__    (S):       return O2sm(S,'__neg__')
    def __pos__    (S):       return O2sm(S,'__pos__')
    def __abs__    (S):       return O2sm(S,'__abs__')
    def __invert__ (S):       return O2sm(S,'__invert__')
    def __complex__(S):       return O2sm(S,'__complex__')
    def __int__    (S):       return O2sm(S,'__int__')
    def __float__  (S):       return O2sm(S,'__float__')
    def __index__  (S):       return O2sm(S,'__index__')
    def __round__  (S,*args): return O2sm(S,'__round__',*args)
    def __trunc__  (S):       return O2sm(S,'__trunc__')
    def __floor__  (S):       return O2sm(S,'__floor__')
    def __ceil__   (S):       return O2sm(S,'__ceil__')
    def __enter__  (S):       return O2sm(S,'__enter__')
    def __exit__   (S,*args): return O2sm(S,'__exit__', *args)
    # __match_args__
    # __await__(self)
    # __aiter__(self)
    # __anext__(self)
    # __aenter__(self)
    # __aexit__(self, exc_type, exc_value, traceback)

# What's unsatisfactory about this, is that it depends quite a lot on the data
# model beyond the simple use of overloading.
#
# Satisfactorally this might work with python2 after the point at which we
# supply parsing mechanisms. Because python2 doesn't have the metaclass stuff
# that we are avoiding nor does it have definable operators, which we can
# handle through parsing, replacing, for example, a+b with add(a,b).
# -- 2023-12-24 It seems perhaps operator overloading and metaclasses
# were added at some point in Python 2.7
#
# Looking at this more clearly, we are starting with python and replacing parts
# of the language step by step. Approximately:
#
# CLASS   - how we define classes
# FORM    - how we define functions
# BASIC   - basic types and functions
# PARSE   - parsing
# INTERP  - execution with interpreter
# COMPILE - execution with compilation to basic interpreted code
#
# Then we move on to other targets and interoperability.
#


# So now having spoilt the class system, we will begin to introduce our own
# type system which will be expressible in our new language.

o2type={}
o2value={}

def O2sm(o,opkey,*args):
    if opkey=='__getattr__':
        if o in o2type:
            t2=o2type[o]
            return t2(o,opkey)(*args)
    if o in o2type:
        t2=o2type[o]
        return t2(o,opkey)(*args)
    raise AttributeError

#     >>> from form import *
#     >>> o=O2()
#     >>> o2type[o]=1
#     >>> {}[o]=1
#     >>> 

# Native type encapsulates python object.
def Native(o,opkey):
    v=o2value[o]
    if opkey=='__setattr__':
        return lambda key,value: setattr(v,key,value)
    if opkey=='__getattr__':
        return lambda key:getattr(v,key)
    return lambda *args:getattr(v,opkey)(*args)


def V(x):
    if isinstance(x,O2): return x
    v=O2()
    o2type[v]=Native
    o2value[v]=x
    return v

#     >>> v2=V(2)
#     >>> v2
#     2
#     >>> class C: pass
#     ... 
#     >>> c=C()
#     >>> c.a=1
#     >>> c2=V(c)
#     >>> c2.a
#     1
#     >>> c2.b=2
#     >>> c.b
#     2
#     >>> c.a=3
#     >>> c.a
#     3
#     >>> c2.a
#     3
#     >>> V(2)*3
#     6
#     >>> 2*V(3)
#     6
#     >>> V(2)*V(3)
#     <67>:1: TypeError: unsupported operand type(s) for *: 'O2' and 'O2'
#     >>> (2).__mul__(V(3))
#     NotImplemented
#     >>> 

# Here we see the work we have cut out for ourselves. We can pass through
# attribute access but it's up to us now to define how our objects and the
# wrapped Native objects interact with native objects. The native objects
# methods also expect native objects, not Native objects. So we have to be
# careful about what universe we are in.
#
# So for example V(2)*V(3) invokes __mul__ on V(2), which in turn tries
# 2.__mul__(V(3)) which it doesn't know how to handle, so returns
# NotImplemented, which in turn is returned from invocation of __mul__ on V(2)
# which then reports the TypeError.
#
# Now we're free to redefine __mul__, but first let us set up the mechanisms
# that allow us to define and redefine our type behaviour more readily.

# We may need something like this to handle weakrefs
# import weakref
# o2type=weakref.WeakKeyDictionary()
# o2value=weakref.WeakKeyDictionary()

# Retrospectively, it seems this approach had some mistakes.
# Give away clues:
# "... having spoilt the class system,... "
# "There's some decisions that may need revising ..."
#
# It seems I was trying to skip to many steps and to do too much at the
# same time and it has not made things straight forward.
#
#     The thoughts of the diligent tend only to plenteousness; but of
#     every one that is hasty only to want. (Proverb)
#
# I had also stopped here to try and pin down the definition of the
# language I am implementing, so that implementing it would be more
# straight forward. I recognise through this a basic working practice:
# 
#     Make implementation as easy and straight forward as possible, at
#     least until the design is pinned down.
#
# Breaking Python's type system goes against its use. That lacks
# synergy. I rather now prefer to construct Python classes in what I
# feel is a more natural way, and to allow time to explore metaclass
# stuff without rushing.
#
# In the end I suspect I may not need classes in the same way, but as a
# matter of taking things a step at a time, the investigation is
# natural. And it would be useful to have a convenient way of defining
# classes in Python for other uses.

# A Renewed Approach is to take each novel aspect of the language and
# explore the ideas separately, which I plan to do in separate files.
# Secondly to assemble them once it's clear how to put them together.
#
# An unstated principle has been Refinement. We work the ideas to get
# the best ideas, the code to get the best code, and the language to get
# the best language.
#
# So to proceed, I will leave this investigation of form here for a
# while and break out ideas into separate files.
